export interface ClothesItem {
  description: string;
  name: string;
}
type content = 'happy' | 'sad' | 'anger' | 'fear' | 'surprise' | 'disgust';

export interface ResponseData {
  // clothes: ClothesItem[];
  content: content;
  // today: number;
}

export interface RequestData {
  // gender: 'man' | 'woman';
  content: string;
}
