export const config = {
  runtime: 'edge',
};
const hostUrl = `https://app.typekey.cn`;


export default async (req: Request) => {
  if (process.env.NODE_ENV === 'development') {
    const { createGatewayOnEdgeRuntime } = await import('@lobehub/chat-plugins-gateway');

    return createGatewayOnEdgeRuntime()(req);
  }

  return new Response('gateway');
};
