import { PluginErrorType, createErrorResponse } from '@lobehub/chat-plugin-sdk';

// import { manClothes, womanClothes } from '@/data';
// import { RequestData, ResponseData } from '@/type';

export const config = {
  runtime: 'edge',
};

export default async (req: Request) => {
  if (req.method !== 'POST') return createErrorResponse(PluginErrorType.MethodNotAllowed);

  // const { gender, content } = (await req.json()) as RequestData;
  const { content } = (await req.json());

  // const clothes = gender === 'man' ? manClothes : womanClothes;

  const result = {
    // clothes: content ? clothes[content] : Object.values(clothes).flat(),
    content,
    // today: Date.now(),
  };

  return new Response(JSON.stringify(result));
};
