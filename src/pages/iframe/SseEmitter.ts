export class SseEmitter {
  private eventSource: EventSource | null = null;

  constructor(private url: string) {}

  public start(): void {
    if (this.eventSource) {
      this.close();
    }
    this.eventSource = new EventSource(this.url);
  }

  public close(): void {
    if (this.eventSource) {
      this.eventSource.close();
      this.eventSource = null;
    }
  }

  public on(eventName: string, handler: (event: MessageEvent) => void): void {
    if (this.eventSource) {
      this.eventSource.addEventListener(eventName, handler);
    }
  }

  public off(eventName: string, handler: (event: MessageEvent) => void): void {
    if (this.eventSource) {
      this.eventSource.removeEventListener(eventName, handler);
    }
  }
}
