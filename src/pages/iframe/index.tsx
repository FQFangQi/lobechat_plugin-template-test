import { lobeChat } from '@lobehub/chat-plugin-sdk/client';
import { useReactive } from 'ahooks';
import { memo, useEffect, useState } from 'react';

// import { Center } from 'react-layout-kit';
import Data from '@/components/Render';
// import { fetchClothes } from '@/services/clothes';
import { ResponseData } from '@/type';

import { SseEmitter } from './SseEmitter';

const hostUrl = `https://app.typekey.cn`;
/** JSON 解析 */
export const safeJSONParse = (value: string) => {
  try {
    return JSON.parse(value);
  } catch (error) {
    console.error('safeJSONParsexxx：', error);
    return null;
  }
};
const RenderFQ = memo(() => {
  // 初始化渲染状态
  const [data, setData] = useState<ResponseData>();
  //用户输入框
  const [myContent, setMyContent] = useState('');
  // 当前会话id
  const [currSessionId, setCurrSessionId] = useState<string | undefined>(undefined);
  //当前系统正在讲的话,实时渲染
  const assistantIngText = useReactive({
    value: '',
  });

  // 初始化时从主应用同步状态
  useEffect(() => {
    lobeChat.getPluginMessage().then(setData);
  }, []);

  // 记录请求参数
  const [payload, setPayload] = useState<any>();

  useEffect(() => {
    lobeChat.getPluginPayload().then((payload) => {
      console.log('payloadxxxuseEffect', payload);
      if (payload?.name === 'content') {
        setPayload(payload.arguments);
      }
    });
  }, []);

  // const fetchData = async () => {
  //   const data = await fetchClothes(payload);
  //   setData(data);
  //   lobeChat.setPluginMessage(data);
  // };

  /**聊天 */
  const handleChat = async () => {
    const sendText = payload?.content;
    if (payload?.content === undefined) {
      return;
    }
    console.log('sendText::', payload);
    /**
     * 1.sendText有值则保存
     * 2.sendText没值则不保存
     * 3.sendText 为undefind 且myContent有值
     */
    if (sendText && sendText.trim() !== '') {
      //将点击按钮的sendText也保存到历史中
      setMyContent(sendText ? sendText : myContent);
    }

    // 创建一个 SSE Emitter 实例
    const emitter = new SseEmitter(
      // 1779803302083563520/%E8%BE%93%E5%87%BA%E5%86%85%E5%AE%B9
      hostUrl +
        encodeURI(
          `/typekey/api/v1/flow/testFlow?flowId=1779803302083563520&content=${sendText}${
            currSessionId ? '&sessionId=' + currSessionId : ''
          }`,
        ),
    );
    // 启动 SSE 连接
    emitter.start();
    // 注册事件处理函数
    emitter.on('message', (event) => {
      // {"sessionId":"613651563156855","replyResultType":"DONE","content":null}
      const { sessionId, replyResultType, content } = safeJSONParse(event.data);
      if (!currSessionId && sessionId) {
        setCurrSessionId(sessionId);
      }

      switch (replyResultType) {
        case 'DONE': {
          emitter.close();
          return;
        }
        case 'TEXT': {
          //文字
          if (content && content !== '') {
            //当前系统正在讲的话,实时渲染
            assistantIngText.value = assistantIngText.value + content;
          }

          break;
        }
        case 'ERROR': {
          console.error('ERRORxxx', content);

          break;
        }
        // No default
      }
    });
    emitter.on('error', (event) => {
      console.error('SSE error:', event);
      if (emitter) {
        emitter.close();
      }
    });
  };
  useEffect(() => {
    if (payload?.content) {
      handleChat();
    }
  }, [payload]);
  return (
    <>
      {/* <Button onClick={()=>{
    console.log("xxx",'点击发送');
    handleChat()
  }}>点击发送</Button> */}
      <h1>{payload?.content}</h1>
      <hr />
      <p>{assistantIngText.value}</p>
      {/* <Data /> */}
    </>
  );
});

export default RenderFQ;
