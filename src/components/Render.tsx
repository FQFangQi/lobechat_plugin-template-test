import { Card } from 'antd';
import { createStyles } from 'antd-style';
import dayjs from 'dayjs';
import { memo } from 'react';
import { Flexbox } from 'react-layout-kit';

import { ResponseData } from '@/type';

// const useStyles = createStyles(({ css, token }) => ({
//   date: css`
//     color: ${token.colorTextQuaternary};
//   `,
// }));

const Render = memo<Partial<ResponseData>>(({ content }) => {

  return (
    <Flexbox gap={24}>
        ::{content}
    </Flexbox>
  );
});

export default Render;
